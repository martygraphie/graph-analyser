import wx
import wx.adv
import wx.grid
import wx.lib.mixins.inspection as wit
from wx.lib.agw import aui

from src.Panel.DataPanel import DataPanel
from src.Panel.DisplayPanel import DisplayPanel
from src.Panel.ResultPanel import ResultPanel
from src.Record import Record


class Main(wx.Frame):
    def __init__(self, *args, **kwargs):
        super().__init__(size= wx.GetClientDisplayRect().Size, *args, **kwargs)
        self.SetMinSize((600, 400))
        self.SetIcon(wx.Icon("src/resources/lpnc.ico"))
        # MENU BAR #
        self.m_menubar = wx.MenuBar(0)
        self.m_menu3 = wx.Menu()
        self.m_menuItem1 = wx.MenuItem(self.m_menu3, wx.ID_OPEN, u"Ouvrir", wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu3.Append(self.m_menuItem1)

        self.m_menu3.AppendSeparator()

        self.m_menuItem2 = wx.MenuItem(self.m_menu3, wx.ID_EXIT, u"Quitter", wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu3.Append(self.m_menuItem2)

        self.m_menubar.Append(self.m_menu3, u"Fichier")

        self.m_menu4 = wx.Menu()
        self.m_menubar.Append(self.m_menu4, u"Aide")

        self.SetMenuBar(self.m_menubar)
        # END MENU BAR #
        self.record = Record()
        # Create an AUI Manager and tell it to manage this Frame
        self._manager = aui.AuiManager()
        self._manager.SetManagedWindow(self)

        self.data_panel = DataPanel(self, self.record)
        self.data_panel_info = aui.AuiPaneInfo().Name('data_panel').Caption('Données Brute').Left(). \
            CloseButton(False).MinimizeButton(True).Show().Floatable(False)

        self.display_panel = DisplayPanel(self,  self.record)
        self.display_panel_info = aui.AuiPaneInfo().Name('display_panel').CenterPane(). \
            CloseButton(False).MinimizeButton(False).Show().Floatable(False)

        self.results_panel = ResultPanel(self, self.record)
        self.results_panel_info = aui.AuiPaneInfo().Name('results_panel').Caption('Calculs').Right(). \
            CloseButton(False).MinimizeButton(True).Show().Floatable(False)

        self._manager.AddPane(self.data_panel, self.data_panel_info)
        self._manager.AddPane(self.display_panel, self.display_panel_info)
        self._manager.AddPane(self.results_panel, self.results_panel_info)
        self._manager.Update()
        frequence_hz = int(self.results_panel.settings_panel.radio_frequence.GetStringSelection())

        self.record.set_data(frequence_hz)

        self.Bind(wx.EVT_MENU, self.on_exit, id=wx.ID_EXIT)
        self.Bind(wx.EVT_MENU, self.on_open, id=wx.ID_OPEN)

    def __on_quit(self, evt):
        self._manager.UnInit()
        del self._manager
        self.Destroy()

    def on_open(self, evt):
        dlg = wx.FileDialog(self, 'Choisissez un fichier', wildcard="Fichier (*.*)|*.*| Fichier CSV (*.csv)|*.csv| Fichier Texte (*.txt)|*.txt", style=wx.FD_OPEN)
        back = dlg.ShowModal()
        path = dlg.GetPath()
        dlg.Destroy()

        if back == wx.ID_OK and path != "":
            self.open_and_draw(path)

    def open_and_draw(self, path):
        self.record.open_file(path)
        self.data_panel.fill_grid()
        self.results_panel.infos_panel.get_info()
        self.display_panel.set_data_2_display()

    def on_exit(self, evt):
        self.__on_quit(evt)


class App(wx.App, wit.InspectionMixin):
    def OnInit(self):
        self.Init()
        win = Main(parent=None, title='Graph Analyser')
        win.Show(True)
        self.SetTopWindow(win)
        return True


if __name__ == '__main__':
    app = App(0)
    app.MainLoop()

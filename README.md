# Graph Analyser

Graph Analyser is a software to analyze the handwriting and interpret the results.

## Prerequisite
Python : 3.8.2
 
## Installation
### Linux

```
$ sudo apt-get install python3-wxgtk4.0 python3-wxgtk-webview4.0 python3-wxgtk-media4.0 python3-scipy python3-panda
```

 Wx not work fine on linux

### Windows

#### Anaconda 


Installing Anaconda 

Import **environment.yml** file from project's as a new virtual environment on Anaconda. 

Configure the switch in your IDE, to use the anaconda environment previously created.  

## Generate Executable

For update executable you juste need to regenerate the build repository 

```
pyinstaller "Graph Analyser.spec"
```
if you have remove Graph Analyser.spec you need to rerun this command : 
```
pyinstaller Main.py -i src/resources/lpnc.ico --add-data src/resources;src/resources -w
```

/!\ if you use Anaconda think to remove forge scipy and numpy and resinstall it on pip (forge not work with pyinstaller)
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
import os
import pandas as pd
from src.Utils import get_type, get_n, get_txt_id


class Subject:

    def __init__(self):
        self.ID = None
        self.age = None
        self.grade = None
        self.session_date = None
        self.laterality = None
        self.gender = None
        self.annotation = None
        self.tablet = None
        self.pool_ref = None
        self.pool = None
        self.filename = None

    def set(self, path):
        if os.path.isfile(path):
            type_of_file = get_type(path).lower()
            self.filename = os.path.basename(path)
            n_stimuli = get_n(self.filename, type_of_file)
            self.n_stimuli = n_stimuli
            if type_of_file == 'csv':
                self.get_infos_csv(path, n_stimuli)
            elif type_of_file == 'txt':
                self.get_infos_txt(path, n_stimuli)
            else:  # if other type will be implemented
                pass

    def get_infos_csv(self, path, n_stimuli):
        # Get the first part of the infos
        info1_path = os.path.dirname(path) + os.path.sep + "info.csv"
        if os.path.isfile(info1_path):
            info1 = pd.read_csv(info1_path)
            info1.columns = info1.columns.str.replace(' ', '')  # remove unwanted space in column names
            try:
                self.grade = info1['Subject_Class'][0]
            except ImportError:
                pass
            try:
                self.tablet = info1['Input_Tool'][0]
            except ImportError:
                pass
            try:
                self.age = info1['Subject_Age_Days'][0]
            except ImportError:
                pass
            try:
                self.session_date = info1['Session_Date'][0][:10]
            except ImportError:
                pass
            try:
                self.pool_ref = info1['Reference_Pool'][0]
            except ImportError:
                pass

        # Get the second part of the infos
        info2_path = os.path.dirname(os.path.dirname(path)) + os.path.sep + "info.csv"
        if os.path.isfile(info2_path):
            info2 = pd.read_csv(info2_path)
            info2.columns = info2.columns.str.replace(' ', '')  # remove unwanted space in column names
            try:
                self.ID = info2['Subject_ID'][0]
            except ImportError:
                pass
            try:
                self.gender = info2['Gender'][0]
            except ImportError:
                pass
            try:
                self.laterality = info2['Laterality'][0]
            except ImportError:
                pass
            try:
                self.annotation = info2['Annotation'][0]
            except ImportError:
                pass

        stimulus_path = os.path.dirname(__file__) + os.path.sep + "resources/stimulus" + os.path.sep
        if self.pool_ref is not None:
            str_pool_ref = "{:03d}".format(self.pool_ref)
            str_grade = 'GS' if self.pool_ref < 4 else ('ISKN' if self.pool_ref == 12 else 'grands')

            file_number = ''
            if self.pool_ref < 4:
                file_number = str(self.pool_ref + 5)
            elif self.pool_ref < 8:
                file_number = str(self.pool_ref - 3)
            elif self.pool_ref < 12:
                file_number = str(self.pool_ref - 7)

            self.stim_list = pd.read_csv(stimulus_path + str_pool_ref + '_' + str_grade + file_number + ".csv")
            self.stim_list = self.stim_list['Content']
            self.n_stimuli = n_stimuli if int(n_stimuli) in self.stim_list.index else str(self.stim_list[ self.stim_list == 'Autres'].index.values[0])
            self.pool = self.stim_list[int(self.n_stimuli)]

    def get_infos_txt(self, path, n_stimuli):
        stimulus_path = os.path.dirname(__file__) + os.path.sep + "resources/stimulus" + os.path.sep
        self.pool_ref = n_stimuli
        self.stim_list = pd.read_csv(stimulus_path + "txt.csv")
        self.stim_list = self.stim_list['Content']
        self.n_stimuli = n_stimuli if int(n_stimuli) in self.stim_list.index else '2'
        self.pool = self.stim_list[int(self.n_stimuli)]
        self.ID = get_txt_id(path)
        self.age = str("Inconnu")
        self.tablet = 0
        self.annotation = 0
        self.gender = 0
        self.laterality = 0
        self.session_date = str("Inconnu")
        self.grade = 0

    def print_infos(self):
        print("ID :", self.ID)
        print("Age :", self.age)
        print("Classe :", self.grade)
        print("Date :", self.session_date)
        print("Latéralité :", self.laterality)
        print("Genre :", self.gender)
        print("Annotation :", self.annotation)
        print("Tablette :", self.tablet)
        print("Stim ref :", self.pool_ref)
        print("Stim :", self.pool)

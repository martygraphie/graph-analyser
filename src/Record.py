"""
Parameters
------------

time_stamp : current time for each point

is_empty : record has sufficient information

passed : true if the person has done the stimuli

strokes : list of strokes

lifts : list of lifts

nb_strokes : number of strokes

nb_lifts : number of lifts

concat_strokes : all strokes gathered

barycenter : the position of the barycenter of the drawing

centered : all strokes centered and gathered

stimuli_name : name of the simuli

subject_ID : ID of the subject

subject_age : Age (days) of the subject

subject_laterality : laterality of the subject (0 for righ handed, 1 for left)

subject_gender : gender of the subject (0 for boy, 1 for girl)

subject_annotation : annotation of the subject (0 for unknown 1 for dysgraphic, 2 for no dysgraphic, 3 for typical)

tool_ID = ID of the tablet used for the session

filter : 'No filter' if the record has not been filtered, name of the filter if it has been

speeds : speed vector for every stroke. Calculated in the filters (speeds[:,0]:timestamps, speeds[:,1]:norm of the
         speed vector, speeds[:,2]:x coordinate of speed vector, speeds[:,3]:y coordinate of speed vector)

accs : acceleration vector for every stroke. Calculated in the filters (cf speeds for description)

strokes_nf : unfiltered strokes

lifts_nf : unfiltered lifts

concat_strokes_nf = unfiltered concat_strokes

------
"""
from src.DataRecord import DataRecord


class Record:
    def __init__(self):
        self.data = DataRecord()

    def set_data(self,frequence_hz):
        self.data.set_frequence_hz(frequence_hz)

    def open_file(self, path):
        self.data.init_record(path)

    def data(self):
        return self.data

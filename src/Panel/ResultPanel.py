import wx


class ResultPanel(wx.Panel):
    # This first class creates a three pages notebook (Infos, Parametres et Resultats)
    def __init__(self, parent,record):
        from src.Panel.InfosPanel import InfosPanel
        from src.Panel.SettingsPanel import SettingsPanel
        from src.Panel.CalculsPanel import CalculsPanel
        super(ResultPanel, self).__init__(parent)  # Initialisation of the class with the main as parent
        self.__parent__ = parent
        self.record = record

        self.SetInitialSize((400, 400))
        self.SetMinSize((400, 400))

        sizer = wx.BoxSizer(wx.VERTICAL)

        self.notebook = wx.Notebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                    wx.NB_TOP)  # Creation of the Notebook

        self.infos_panel = InfosPanel(self.notebook,record)  # Initialisation of the three pages
        self.settings_panel = SettingsPanel(self.notebook,record)
        self.results_panel = CalculsPanel(self.notebook,record)

        self.notebook.AddPage(self.infos_panel, u"Infos", False)  # Creation of the three pages
        self.notebook.AddPage(self.settings_panel, u"Parametres", False)
        self.notebook.AddPage(self.results_panel, u"Resultats", False)

        sizer.Add(self.notebook, 1, wx.EXPAND | wx.ALL, 5)  # Addition of the notebook to the interface

        self.SetSizer(sizer)
        self.Layout()
        sizer.Fit(self)

    # This function serves as a link between the InfosPanel class and the main when you want to open a new file
    # based on a stimulus. We are searching the parent of the InfosPanel class and then the parent of
    # the notebook in order to call a function in the main
    def new_open(self, chemin):
        self.__parent__.GetParent().__parent__.open_and_draw(chemin)

    # This function serves as a link between the InfosPanel class and the CalculsPanel class
    # In the InfosPanel class, the user can call for a calculation so this function will call the function in the
    # CalculsPanel class
    def calcul(self, debut, fin, snvpd_first_frequence,snvpd_second_frequence):
        self.__parent__.GetParent().results_panel.calcul(debut, fin, snvpd_first_frequence,snvpd_second_frequence)

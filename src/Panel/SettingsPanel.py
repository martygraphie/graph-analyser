import wx

from src.Panel.ResultPanel import ResultPanel


class SettingsPanel(ResultPanel, wx.Panel):
    # This class handle the settings
    def __init__(self, parent,record):
        super(wx.Panel, self).__init__(parent)
        self.__parent__ = parent
        self.cursor_start = 0
        self.cursor_end = -1
        self.record = record
        sizer = wx.BoxSizer(wx.VERTICAL)

        radio_pressurChoices = [u"0", u"0.01", u"0.1", u"1", u"10"]
        self.radio_pressur = wx.RadioBox(self, wx.ID_ANY, u"Epsilon pression", wx.DefaultPosition, wx.DefaultSize,
                                         radio_pressurChoices, 1,
                                         wx.RA_SPECIFY_ROWS)  # Creation of a slider with 5 possible postions
        self.radio_pressur.SetSelection(2)
        sizer.Add(self.radio_pressur, 0, wx.ALL | wx.EXPAND, 5)

        radio_vitesseChoices = [u"0", u"0.0001", u"0.001", u"0.01", u"0.1"]
        self.radio_vitesse = wx.RadioBox(self, wx.ID_ANY, u"Epsilon vitesse (cm/s)", wx.DefaultPosition, wx.DefaultSize,
                                         radio_vitesseChoices, 1, wx.RA_SPECIFY_ROWS)
        self.radio_vitesse.SetSelection(2)
        sizer.Add(self.radio_vitesse, 0, wx.ALL | wx.EXPAND, 5)

        radio_accelerationChoices = [u"0", u"0.1", u"1", u"10", u"100"]
        self.radio_acceleration = wx.RadioBox(self, wx.ID_ANY, u"Epsilon accélération(cm/s²)", wx.DefaultPosition,
                                              wx.DefaultSize, radio_accelerationChoices, 1, wx.RA_SPECIFY_ROWS)
        self.radio_acceleration.SetSelection(2)
        sizer.Add(self.radio_acceleration, 0, wx.ALL | wx.EXPAND, 5)

        radio_frequence_choices = [u"5", u"10", u"15"]
        self.radio_frequence = wx.RadioBox(self, wx.ID_ANY, u"Fréquence (Hz)", wx.DefaultPosition,
                                              wx.DefaultSize, radio_frequence_choices, 1, wx.RA_SPECIFY_ROWS)
        self.radio_frequence.SetSelection(2)
        sizer.Add(self.radio_frequence, 0, wx.ALL | wx.EXPAND, 5)

        self.radio_frequence_first = wx.RadioBox(self, wx.ID_ANY, u"SNVPD Premier Profil - Fréquence (Hz)", wx.DefaultPosition,
                                              wx.DefaultSize, radio_frequence_choices, 1, wx.RA_SPECIFY_ROWS)
        self.radio_frequence_first.SetSelection(1)
        sizer.Add(self.radio_frequence_first, 0, wx.ALL | wx.EXPAND, 5)

        self.radio_frequence_second = wx.RadioBox(self, wx.ID_ANY, u"SNVPD Second Profil - Fréquence (Hz)", wx.DefaultPosition,
                                              wx.DefaultSize, radio_frequence_choices, 1, wx.RA_SPECIFY_ROWS)
        self.radio_frequence_second.SetSelection(0)
        sizer.Add(self.radio_frequence_second, 0, wx.ALL | wx.EXPAND, 5)

        self.button_clear = wx.Button(self, wx.ID_ANY, u"Réinitialiser les marqueurs", wx.DefaultPosition, wx.DefaultSize,
                                     0)  # ♣This button allows to select the position of the cursor on the drawing as
        # the starting point of the calculation
        sizer.Add(self.button_clear, 0, wx.ALL, 5)

        self.buttonstart = wx.Button(self, wx.ID_ANY, u"Marquer début", wx.DefaultPosition, wx.DefaultSize,
                                     0)  # ♣This button allows to select the position of the cursor on the drawing as
        # the starting point of the calculation
        sizer.Add(self.buttonstart, 0, wx.ALL, 5)

        self.buttonend = wx.Button(self, wx.ID_ANY, u"Marquer fin", wx.DefaultPosition, wx.DefaultSize,
                                   0)  # ♣This button allows to select the position of the cursor on the drawing as
        # the end point of the calculation
        sizer.Add(self.buttonend, 0, wx.ALL, 5 )

        self.buttoncalcul = wx.Button(self, wx.ID_ANY, u"Debut calcul", wx.DefaultPosition, wx.DefaultSize,
                                      0)  # This button will start the calculations, the button is binded to the
        # function "button"
        sizer.Add(self.buttoncalcul, 0, wx.ALL, 5)

        self.Bind(wx.EVT_BUTTON, self.mark_clear, self.button_clear)
        self.Bind(wx.EVT_BUTTON, self.mark_start, self.buttonstart)
        self.Bind(wx.EVT_BUTTON, self.mark_end, self.buttonend)
        self.Bind(wx.EVT_BUTTON, self.lunch_calcul, self.buttoncalcul)

        self.SetSizer(sizer)
        self.Layout()
        sizer.Fit(self)

    def mark_clear(self, evt):
        self.cursor_start = 0
        self.cursor_end = -1

    def mark_start(self, evt):
        self.cursor_start = wx.GetTopLevelParent(self).display_panel.cursor_index

    def mark_end(self, evt):
        c_i = wx.GetTopLevelParent(self).display_panel.cursor_index
        if c_i < self.cursor_start:
            self.cursor_end = self.cursor_start
            self.cursor_start = c_i
        elif c_i > self.cursor_start:
            self.cursor_end = c_i

    def lunch_calcul(self, evt):
        print(self.cursor_start, self.cursor_end)
        # This function is called when the button buttoncalcul is activated
        # We are calling the function calcul in the ResultPanel class
        snvpd_frequence_first = int(self.radio_frequence_first.GetStringSelection())
        snvpd_frequence_second = int(self.radio_frequence_second.GetStringSelection())
        super(SettingsPanel, self).calcul(self.cursor_start, self.cursor_end, snvpd_frequence_first, snvpd_frequence_second)
        message = "Le calcul de l'ensemble" if self.cursor_start == 0 and self.cursor_end == -1 else "Le calcul compris entre les points " + str(self.cursor_start) + " et " + str(self.cursor_end)
        message = message + " du stimuli est terminé. Pour accèder aux resultats, reportez-vous à l'onglet \"Résulats\"."
        wx.MessageBox(message, "Message", wx.OK | wx.ICON_INFORMATION)

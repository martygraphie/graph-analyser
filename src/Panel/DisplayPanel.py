from copy import deepcopy
from wx.lib.agw import aui
from src.Canvas import *
from src import Record
import wx
from math import sqrt
import numpy as np

flatten = lambda l: [item for sublist in l for item in sublist]


class DisplayPanel(wx.Panel):
    def __init__(self, parent, record):
        super(DisplayPanel, self).__init__(parent)
        self.__parent__ = parent
        self.record = record
        self.cursor_index = 0

        # TOOLS BAR #
        t_size = 30

        tb = wx.ToolBar(self, style=wx.TB_DEFAULT_STYLE)
        tb_info = aui.AuiPaneInfo().Name('toolbar').Top().CaptionVisible(False).Resizable(False).CloseButton(
            False).MinimizeButton(False).Show().Floatable(False).MinSize(wx.Size(-1, t_size))

        write_icon = wx.Bitmap(wx.Image('src/resources/img/write.png').Scale(t_size, t_size, wx.IMAGE_QUALITY_HIGH))
        cursor_icon = wx.Bitmap(wx.Image('src/resources/img/cursor.png').Scale(t_size, t_size, wx.IMAGE_QUALITY_HIGH))
        self.zoom_in_icon = wx.Bitmap(
            wx.Image('src/resources/img/zoom_in.png').Scale(t_size, t_size, wx.IMAGE_QUALITY_HIGH))
        self.zoom_out_icon = wx.Bitmap(
            wx.Image('src/resources/img/zoom_out.png').Scale(t_size, t_size, wx.IMAGE_QUALITY_HIGH))

        redraw_btn = tb.AddTool(wx.ID_ANY, "redraw", write_icon, shortHelp="Redraw", kind=wx.ITEM_NORMAL)
        tb.AddSeparator()
        cursor_btn = tb.AddTool(0, "curseur", cursor_icon, shortHelp="Curseur", kind=wx.ITEM_RADIO)
        zoom_in_btn = tb.AddTool(2, "zoom in", self.zoom_in_icon, shortHelp="Zoom In", kind=wx.ITEM_RADIO)
        zoom_out_btn = tb.AddTool(3, "zoom out", self.zoom_out_icon, shortHelp="Zoom Out", kind=wx.ITEM_RADIO)
        tb.Realize()

        self.tool_selected = 0
        tb.Bind(wx.EVT_TOOL, self.on_redraw, redraw_btn)
        tb.Bind(wx.EVT_TOOL, self.on_tool_press, cursor_btn)
        tb.Bind(wx.EVT_TOOL, self.on_tool_press, zoom_in_btn)
        tb.Bind(wx.EVT_TOOL, self.on_tool_press, zoom_out_btn)
        # END TOOLS BAR #

        # Create an AUI Manager and tell it to manage this Frame
        self._manager = aui.AuiManager()
        self._manager.SetManagedWindow(self)

        self.script_panel = HandWritingPanel(self, self.record)
        self.script_panel_info = aui.AuiPaneInfo().Name('script_panel').Caption('Écriture').Center(). \
            CloseButton(False).MinimizeButton(False).Show().Floatable(False)

        self.alti_panel = PressurePanel(self, self.record)
        self.alti_panel_info = aui.AuiPaneInfo().Name('alti_panel').Caption('Pression').Center(). \
            CloseButton(False).MinimizeButton(True).Show().Floatable(False)

        self.speed_panel = SpeedPanel(self, self.record)
        self.speed_panel_info = aui.AuiPaneInfo().Name('speed_panel').Caption('Vitesse').Center(). \
            CloseButton(False).MinimizeButton(True).Show().Floatable(False)

        #        self._manager.AddPane(self.toolbar, self.toolbar_info )
        self._manager.AddPane(tb, tb_info)
        self._manager.AddPane(self.script_panel, self.script_panel_info)
        self._manager.AddPane(self.alti_panel, self.alti_panel_info)
        self._manager.AddPane(self.speed_panel, self.speed_panel_info)
        self._manager.Update()

    def set_data_2_display(self):
        self.on_redraw(None)

    def on_tool_press(self, evt):
        self.tool_selected = evt.GetId()

        if self.tool_selected == 0:  # cursor
            cursor = wx.CURSOR_CROSS
        elif self.tool_selected == 2:  # zoom in
            cursor = self.zoom_in_icon.ConvertToImage().Scale(20, 20, wx.IMAGE_QUALITY_HIGH)
            cursor.SetOption(wx.IMAGE_OPTION_CUR_HOTSPOT_X, 5)
            cursor.SetOption(wx.IMAGE_OPTION_CUR_HOTSPOT_Y, 5)
        elif self.tool_selected == 3:  # zoom out
            cursor = self.zoom_out_icon.ConvertToImage().Scale(25, 25, wx.IMAGE_QUALITY_HIGH)
            cursor.SetOption(wx.IMAGE_OPTION_CUR_HOTSPOT_X, 5)
            cursor.SetOption(wx.IMAGE_OPTION_CUR_HOTSPOT_Y, 5)

        self.script_panel.SetCursor(wx.Cursor(cursor))
        self.alti_panel.SetCursor(wx.Cursor(cursor))
        self.speed_panel.SetCursor(wx.Cursor(cursor))

    def on_redraw(self, evt):
        if not self.record.data.is_empty:
            self.script_panel.zoom_factor = 1
            self.script_panel.set_compute_draw()

            self.alti_panel.zoom_factor = 1
            self.alti_panel.set_compute_draw()

            self.speed_panel.zoom_factor = 1
            self.speed_panel.set_compute_draw()

    def set_cursors(self, i, select_row=True):
        if i >= self.record.data.size:
            i = self.record.data.size - 1
        elif i < 0:
            i = 0

        self.cursor_index = i

        self.script_panel.set_cursor(i)
        self.alti_panel.set_cursor(i)
        self.speed_panel.set_cursor(i)

        if select_row:
            self.__parent__.data_panel.raw_data_grid.SelectRow(i)


class HandWritingPanel(Canvas):
    s1, s2 = [], []
    cursor_pos = 0, 0
    cursor_index = None
    x_min, y_min, y_max, coefficient = 0, 0, 0, 1

    def __init__(self, parent, record, id=-1, size=wx.DefaultSize):
        Canvas.__init__(self, parent, id)
        self.__parent__ = parent
        self.zoom_factor = 1
        self.Show(True)
        self.record = record
        self.Bind(wx.EVT_LEFT_UP, self.on_click)

    def draw(self, dc):
        dc.Clear()
        if self.s1 != []:
            dc.SetPen(wx.Pen("#CCCCCC", 1))
            for i in range(len(self.s2)):
                points = self.s2[i][:, [0, 1]]
                if len(points) > 2:
                    dc.DrawSpline(points)
                else:
                    for i in range(len(points)) :
                        dc.DrawPoint(points[i][0], points[i][1])
            dc.SetPen(wx.Pen("#0000FF", 1))
            for i in range(len(self.s1)):
                points = self.s1[i][:, [0, 1]]
                if len(points) > 2:
                    dc.DrawSpline(points)
                else:
                    for i in range(len(points)):
                        dc.DrawPoint(points[i][0], points[i][1])    
        if self.cursor_index is not None:
            # X Cursor :
            dc.SetPen(wx.Pen("#FF0000", 1))
            dc.DrawLine((self.cursor_pos[0], 0), (self.cursor_pos[0], 2000))
            # Y Cursor :
            dc.SetPen(wx.Pen("#00FF00", 1))
            dc.DrawLine((0, self.cursor_pos[1]), (2000, self.cursor_pos[1]))

    def set_compute_draw(self):
        # Compute min and max on x and y axes
        self.x_min, self.x_max = self.record.data.raw_data.x.min() * 0.99, self.record.data.raw_data.x.max() * 1.01
        self.y_min, self.y_max = self.record.data.raw_data.y.min() * 0.99, self.record.data.raw_data.y.max() * 1.01
        # Compute the coef of the drawing depending on the panel size and zoom  (for scaling properly)
        self.coefficient = self.GetSize()[1] / (self.y_max - self.y_min) * self.zoom_factor

        # Set the size of the drawing (for the ScrollBar)
        self.drawingWidth = (self.x_max - self.x_min) * self.coefficient
        self.drawingHeight = (self.y_max - self.y_min) * self.coefficient

        # Scale the strokes
        self.s1 = deepcopy(self.record.data.strokes_nf)
        for s in self.s1:
            s[:, 0] = (s[:, 0] - self.x_min) * self.coefficient
            s[:, 1] = (s[:, 1] - self.y_min) * self.coefficient

        # Scale the lifts
        self.s2 = deepcopy(self.record.data.lifts_nf)
        for s in self.s2:
            s[:, 0] = (s[:, 0] - self.x_min) * self.coefficient
            s[:, 1] = (s[:, 1] - self.y_min) * self.coefficient

        # Compute the cursor position
        if self.cursor_index is not None:
            self.cursor_pos = (self.record.data.raw_data.x[self.cursor_index] - self.x_min) * self.coefficient, \
                              (self.record.data.raw_data.y[self.cursor_index] - self.y_min) * self.coefficient

        # Draw
        self.update()

    def on_click(self, event):
        if self.s1 != []:
            if self.__parent__.tool_selected == 0:  # cursor
                i = self.pos_2_index(event.GetPosition())
                self.__parent__.set_cursors(i)
            else:
                if self.__parent__.tool_selected == 2:  # zoom in
                    if self.zoom_factor < 2:
                        self.zoom_factor += 0.1
                elif self.__parent__.tool_selected == 3:  # zoom out
                    if self.zoom_factor > 0.2:
                        self.zoom_factor -= 0.1
                self.set_compute_draw()
        else:
            pass

    def pos_2_index(self, pos):
        pts = flatten(self.s1) + flatten(self.s2)  # Create an array with all the displayed points
        x, y = pts[0][0], pts[0][1]  # Get the first x, y (for initialization)

        pos = self.CalcScrolledPosition(pos[0], pos[1])  # Calculated the click position on the screen panel
        d_min = sqrt((x - pos[0]) ** 2 + (
                y - pos[1]) ** 2)  # initialize the minimum distant (with a random point, here with the first point)

        # browse on all write point, and save the point closest to the click position. 
        for p in pts:
            d = sqrt((p[0] - pos[0]) ** 2 + (p[1] - pos[1]) ** 2)
            if d < d_min:
                d_min, x, y = d, p[0], p[1]

        # found the index of the saved point
        for i in range(len(self.record.data.raw_data.x)):
            if self.record.data.raw_data.x[i] == x / self.coefficient + self.x_min and self.record.data.raw_data.y[
                i] == y / self.coefficient + self.y_min:
                return i

        return None

    def set_cursor(self, i):
        self.cursor_index = i
        self.set_compute_draw()


class PressurePanel(Canvas):
    press = []
    cursor_pos = 0
    cursor_index = None

    def __init__(self, parent, record, id=-1, size=wx.DefaultSize):
        Canvas.__init__(self, parent, id, scroll=False)
        self.__parent__ = parent
        self.zoom_factor = 1
        self.Show(True)
        self.record = record
        self.Bind(wx.EVT_LEFT_UP, self.on_click)

    def draw(self, dc):
        dc.Clear()
        if self.press != []:
            dc.SetPen(wx.Pen("#000000", 1))
            dc.DrawSpline(self.press)

        if self.cursor_index is not None:
            dc.SetPen(wx.Pen("#FF0000", 1))
            dc.DrawLine((self.cursor_pos, 0), (self.cursor_pos, 2000))

    def set_compute_draw(self):
        self.press = []  # Reset the buf
        press_max, press_min = max(self.record.data.pressure), min(self.record.data.pressure)

        p_size = self.GetSize()
        p_size = [p_size[0] - 10, p_size[1] - 10]
        if p_size[0] * self.zoom_factor > p_size[0]: p_size[1] -= 30

        for i in range(self.record.data.size):
            self.press.append([5 + i / self.record.data.size * p_size[0] * self.zoom_factor, \
                               5 + p_size[1] - self.record.data.pressure[i] * p_size[1] / (press_max - press_min)])

        self.drawingWidth, self.drawingHeight = max([row[0] for row in self.press]), max([row[1] for row in self.press])

        # Compute the cursor position
        if self.cursor_index is not None:
            self.cursor_pos = 5 + self.cursor_index / self.record.data.size * (p_size[0] - 10) * self.zoom_factor

        self.update()

    def on_click(self, event):
        if self.press != []:
            if self.__parent__.tool_selected == 0:  # cursor
                i = (event.GetPosition()[0] - 5) * self.record.data.size // self.GetSize()[0]
                self.__parent__.set_cursors(i)
            else:
                if self.__parent__.tool_selected == 2:  # zoom in
                    if self.zoom_factor < 2:
                        self.zoom_factor += 0.1
                elif self.__parent__.tool_selected == 3:  # zoom out
                    if self.zoom_factor > 0.2:
                        self.zoom_factor -= 0.1
                self.set_compute_draw()

    def set_cursor(self, i):
        self.cursor_index = i
        self.set_compute_draw()


class SpeedPanel(Canvas):
    cursor_pos = 0
    cursor_index = None
    speed = []

    def __init__(self, parent, record, id=-1, size=wx.DefaultSize):
        Canvas.__init__(self, parent, id, scroll=False)
        self.__parent__ = parent
        self.record = record
        self.speed = self.record.data.speed
        self.zoom_factor = 1
        self.Show(True)
        self.Bind(wx.EVT_LEFT_UP, self.on_click)

    def set_compute_draw(self):
        self.speed = []
        s_max = self.record.data.speed.max()  # Maximal speed
        p_size = self.GetSize()  # Size of the panel
        p_size = [p_size[0] - 10, p_size[1] - 10]
        if p_size[0] * self.zoom_factor > p_size[0]: p_size[1] -= 30
        for i in range(self.record.data.speed.size - 1):
            self.speed.append([5 + i / self.record.data.size * p_size[0] * self.zoom_factor,
                               5 + p_size[1] - self.record.data.speed[i][0] * p_size[1] / s_max])

        self.drawingWidth, self.drawingHeight = max([row[0] for row in self.speed]), max([row[1] for row in self.speed])

        # Compute the cursor position
        if self.cursor_index is not None:
            self.cursor_pos = 5 + self.cursor_index / self.record.data.size * (p_size[0] - 10) * self.zoom_factor

        self.update()

    def draw(self, dc):
        dc.Clear()
        if self.speed != []:
            dc.SetPen(wx.Pen("#000000", 1))
            dc.DrawSpline(self.speed)

        if self.cursor_index is not None:
            dc.SetPen(wx.Pen("#FF0000", 1))
            dc.DrawLine((self.cursor_pos, 0), (self.cursor_pos, 2000))

    def on_click(self, event):
        if self.speed != []:
            if self.__parent__.tool_selected == 0:  # cursor
                i = (event.GetPosition()[0] - 5) * self.record.data.size // self.GetSize()[0]
                self.__parent__.set_cursors(i)
            else:
                if self.__parent__.tool_selected == 2:  # zoom in
                    if self.zoom_factor < 2:
                        self.zoom_factor += 0.1
                elif self.__parent__.tool_selected == 3:  # zoom out
                    if self.zoom_factor > 0.2:
                        self.zoom_factor -= 0.1
                self.set_compute_draw()

    def set_cursor(self, i):
        self.cursor_index = i
        self.set_compute_draw()

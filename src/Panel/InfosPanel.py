import os
import wx
from src import Record
from src.Panel.ResultPanel import ResultPanel


class InfosPanel(ResultPanel, wx.Panel):
    # This class handles the first panel of the notebook
    def __init__(self, parent,record):
        super(wx.Panel, self).__init__(parent)
        self.__parent__ = parent
        self.record = record
        sizer = wx.FlexGridSizer(0, 2, 0, 0)
        sizer.AddGrowableCol(1)
        sizer.SetFlexibleDirection(wx.BOTH)
        sizer.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        # The next 8 lines are repeated for every items in the info panel
        self.stid = wx.StaticText(self, wx.ID_ANY, u"Identifiant du sujet", wx.DefaultPosition, wx.DefaultSize,
                                  0)  # Creation of a static text (non modifiable text)
        self.stid.Wrap(-1)
        sizer.Add(self.stid, 0, wx.ALL, 5)  # Addition of the text to the interface

        self.tcid = wx.TextCtrl(self, wx.ID_ANY, u"0", wx.DefaultPosition, wx.DefaultSize,
                                0)  # Creation of a text control (a modifiable text)
        sizer.Add(self.tcid, 0, wx.ALL | wx.EXPAND, 5)

        self.stgenre = wx.StaticText(self, wx.ID_ANY, u"Genre", wx.DefaultPosition, wx.DefaultSize, 0)
        self.stgenre.Wrap(-1)
        sizer.Add(self.stgenre, 0, wx.ALL, 5)

        choice0Choices = [u"Inconnu",u"Male", u"Femelle"]
        self.choice0 = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, choice0Choices,
                                 0)  # Creation of a choice panel, here you can choose between male and female
        self.choice0.SetSelection(0)
        sizer.Add(self.choice0, 0, wx.ALL | wx.EXPAND, 5)

        self.staticText2 = wx.StaticText(self, wx.ID_ANY, u"Latéralité", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText2.Wrap(-1)
        sizer.Add(self.staticText2, 0, wx.ALL, 5)

        choice1Choices = [u"Inconnu",u"Droitier", u"Gaucher"]
        self.choice1 = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, choice1Choices, 0)
        self.choice1.SetSelection(0)
        sizer.Add(self.choice1, 0, wx.ALL | wx.EXPAND, 5)

        self.staticText3 = wx.StaticText(self, wx.ID_ANY, u"Annotation", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText3.Wrap(-1)
        sizer.Add(self.staticText3, 0, wx.ALL, 5)

        m_choice31Choices = [u"Inconnu", u"Dysgraphique", u"Non Dysgraphique", u"Typique"]
        self.m_choice31 = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice31Choices, 0)
        self.m_choice31.SetSelection(0)
        sizer.Add(self.m_choice31, 0, wx.ALL | wx.EXPAND, 5)

        self.staticText4 = wx.StaticText(self, wx.ID_ANY, u"Commentaire", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText4.Wrap(-1)
        sizer.Add(self.staticText4, 0, wx.ALL, 5)

        self.tccomment = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        sizer.Add(self.tccomment, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText8 = wx.StaticText(self, wx.ID_ANY, u"Date de la session", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText8.Wrap(-1)
        sizer.Add(self.m_staticText8, 0, wx.ALL, 5)
        self.tcdate = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)

        sizer.Add(self.tcdate, 0, wx.ALL | wx.EXPAND, 5)

        self.m_staticText9 = wx.StaticText(self, wx.ID_ANY, u"Age (jours)", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText9.Wrap(-1)
        sizer.Add(self.m_staticText9, 0, wx.ALL, 5)
        self.tcage = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        # self.textCtrl4.Enable( False )
        sizer.Add(self.tcage, 0, wx.ALL | wx.EXPAND, 5)

        self.stclasse = wx.StaticText(self, wx.ID_ANY, u"Classe", wx.DefaultPosition, wx.DefaultSize, 0)
        self.stclasse.Wrap(-1)
        sizer.Add(self.stclasse, 0, wx.ALL, 5)

        m_choice4Choices = [u"Inconnu", u"PS", u"MS", u"GS", u"CP", u"CE1", u"CE2", u"CM1", u"CM2", u"Sixième", u"Cinquième",
                            u"Quatrième", u"Troisième", u"Seconde", u"Première", u"Terminale"]
        self.m_choice4 = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice4Choices, 0)
        self.m_choice4.SetSelection(0)
        sizer.Add(self.m_choice4, 0, wx.ALL | wx.EXPAND, 5)

        self.stsequence = wx.StaticText(self, wx.ID_ANY, u"Séquence utilisée", wx.DefaultPosition, wx.DefaultSize, 0)
        self.stsequence.Wrap(-1)
        sizer.Add(self.stsequence, 0, wx.ALL, 5)
        self.tcsequence = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        # self.textCtrl4.Enable( False )
        sizer.Add(self.tcsequence, 0, wx.ALL | wx.EXPAND, 5)

        self.ststimulus = wx.StaticText(self, wx.ID_ANY, u"Stimulus", wx.DefaultPosition, wx.DefaultSize, 0)
        self.ststimulus.Wrap(-1)
        sizer.Add(self.ststimulus, 0, wx.ALL, 5)

        self.choice6 = wx.Choice(self)
        self.choice6.SetFont(wx.Font(10, wx.FONTFAMILY_SCRIPT, wx.NORMAL, wx.NORMAL, False, u''))
        self.choice6.Enable(False)
        sizer.Add(self.choice6, 0, wx.ALL | wx.EXPAND, 5)

        self.sttablet = wx.StaticText(self, wx.ID_ANY, u"Tablette", wx.DefaultPosition, wx.DefaultSize, 0)
        self.sttablet.Wrap(-1)
        sizer.Add(self.sttablet, 0, wx.ALL, 5)

        choice5choices = [u"Inconnu",u"ISKN", u"Wacom", u"Wacom Large", u"Wacom 3L", u"Wacom 4M"]
        self.chtablet = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, choice5choices, 0)
        self.chtablet.SetSelection(0)

        sizer.Add(self.chtablet, 0, wx.ALL | wx.EXPAND, 5)

        self.SetSizer(sizer)
        self.Layout()
        sizer.Fit(self)

    def get_info(self):
        # Thif function will get the information about the subject and ass them to the info panel
        # The information are in a global variable called record which is composed of data which is
        # composed of various sub variable but notably the dataframe subject where all the information about the
        # subject are
        self.m_choice4.SetSelection(self.record.data.subject.grade)
        self.m_choice4.Enable(False)
        self.chtablet.SetSelection(self.record.data.subject.tablet)
        self.chtablet.Enable(False)
        self.tcage.SetValue(str(self.record.data.subject.age))
        self.tcage.Enable(False)

        self.tcdate.SetValue(self.record.data.subject.session_date)
        self.tcdate.Enable(False)

        self.tcid.SetValue(self.record.data.subject.ID)
        self.tcid.Enable(False)
        self.choice0.SetSelection(self.record.data.subject.gender)
        self.choice0.Enable(False)
        self.choice1.SetSelection(self.record.data.subject.laterality)
        self.choice1.Enable(False)
        self.m_choice31.SetSelection(self.record.data.subject.annotation)

        self.tcsequence.SetValue(str(self.record.data.subject.pool_ref))
        self.tcsequence.Enable(False)

        # We set the stimulus list using the list that has been used for the session
        self.choice6.SetItems(self.record.data.subject.stim_list)
        self.choice6.Enable(True)
        self.choice6.SetSelection(int(self.record.data.subject.n_stimuli))  # We set the stimulus



        # If we try to change the stimulus, we enter the function on_choice
        self.Bind(wx.EVT_CHOICE, self.on_choice, self.choice6)

    def on_choice(self, evt):
        if self.record.data.type_of_file == 'csv':

            new_path = self.record.data.session_path + os.path.sep + "Record_" + str(
                self.choice6.GetSelection()) + ".csv"  # We generate a path depending on the new stimulus that we have
            # choosen

            super(InfosPanel, self).new_open(
                new_path)  # We call the function on_choice in the ResultsPanel class that will call another function in
        # the main
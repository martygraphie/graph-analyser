import wx
import wx.grid


class DataPanel(wx.Panel):
    def __init__(self, parent,record):
        super(DataPanel, self).__init__(parent)
        self.__parent__ = parent
        self.record = record

        self.SetMinSize((100, 100))

        sizer = wx.BoxSizer(wx.VERTICAL)

        self.raw_data_grid = MyGrid(self,record)
        sizer.Add(self.raw_data_grid, 0, wx.ALL | wx.EXPAND)

        self.SetSizer(sizer)
        self.Layout()
        sizer.Fit(self)


    def set_max_size(self, maximum=wx.Size(-1, -1)):
        self.SetMaxSize(maximum)

    def set_min_size(self, minimum=wx.Size(-1, -1)):
        self.SetMinSize(minimum)

    def set_grid(self, d):
        self.raw_data_grid.creategrid(d)

    def fill_grid(self):
        self.raw_data_grid.fill_grid()


class MyGrid(wx.grid.Grid):
    def __init__(self, parent,record):
        wx.grid.Grid.__init__(self, parent, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0)
        self.__parent__ = parent
        self.record = record
        # Grid
        self.CreateGrid(34, 8)
        self.EnableEditing(False)
        self.EnableGridLines(True)
        self.EnableDragGridSize(False)
        self.SetMargins(0, 0)

        # Columns
        self.EnableDragColMove(False)
        self.EnableDragColSize(True)
        self.SetColLabelSize(30)
        self.SetColLabelAlignment(wx.ALIGN_CENTRE, wx.ALIGN_CENTRE)
        self.AutoSizeColumns()

        # Rows
        self.EnableDragRowSize(True)
        self.SetRowLabelSize(30)
        self.SetRowLabelAlignment(wx.ALIGN_CENTRE, wx.ALIGN_CENTRE)
        self.AutoSizeRows()

        # Cell Defaults
        self.SetDefaultCellAlignment(wx.ALIGN_CENTRE, wx.ALIGN_CENTRE)

        # Bind evt
        self.Bind(wx.grid.EVT_GRID_RANGE_SELECT, self.on_select_range)

    def on_select_range(self, evt):
        if not self.record.data.is_empty and evt.GetTopLeftCoords()[0] == evt.GetBottomRightCoords()[0] and evt.Selecting():
            self.__parent__.__parent__.display_panel.set_cursors(evt.GetTopLeftCoords()[0], select_row=False)
            self.MakeCellVisible(evt.GetTopLeftCoords()[0], evt.GetTopLeftCoords()[1])

    def fill_grid(self):
        self.ClearGrid()
        columns_names = self.record.data.raw_data.columns.tolist()
        if getattr(self, 'grid', 0):
            self.grid.Destroy()
        n_row = self.GetNumberRows()
        n_col = self.GetNumberCols()
        if n_row < len(self.record.data.raw_data):
            self.AppendRows(len(self.record.data.raw_data) - n_row)
        elif n_row > len(self.record.data.raw_data):
            self.DeleteRows(pos=len(self.record.data.raw_data), numRows=len(self.record.data.raw_data) - n_row)
        if n_col < len(columns_names):
            self.AppendCols(len(columns_names) - n_col)
        elif n_col > len(columns_names):
            self.DeleteCols(pos=len(columns_names), numCols=len(columns_names) - n_col)

        # Set column label
        for i in range(len(columns_names)):
            self.SetColLabelValue(i, columns_names[i])

        progress_dialog = wx.ProgressDialog("Patientez s'il vous plait", "Chargement du Fichier", len(self.record.data.raw_data))
        # populate the grid
        for row in range(len(self.record.data.raw_data)):
            for col in range(len(columns_names)):
                self.SetCellValue(row, col, str(round(self.record.data.raw_data.iloc[row, col], 3)))
            wx.YieldIfNeeded()  # to prevent the window from being unresponsive
            progress_dialog.Update(row)
        progress_dialog.Destroy()
        self.AutoSizeColumns()
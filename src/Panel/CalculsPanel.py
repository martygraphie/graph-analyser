import copy
import csv

import wx
import numpy as np
import pandas as pd

from src.Functions import RenyiEntropy, Snvpd, LongStops, Snr, SlowMovements


class CalculsPanel(wx.ScrolledWindow):
    # This class handles all the calculations there is to do, it is a scrolled window
    fields = {}

    def __init__(self, parent, record):
        super(CalculsPanel, self).__init__(parent, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                           wx.HSCROLL | wx.VSCROLL)
        self.record = record
        self.fields_label = {
            "total_length": u"Longueur totale (cm)",
            "total_length_paper": u"Longueur sur le papier (cm)",
            "total_time_paper": u"Temps sur le papier (ms)",
            "total_time_air": u"Temps en l'air (ms)",
            "total_average_speed": u"Vitesse moyenne totale (cm/s)",
            "total_average_speed_paper": u"Vitesse moyenne sur le papier (cm/s)",
            "pressure_average": u"Pression moyenne totale",
            "pressure_average_paper": u"Pression moyenne sur le papier",
            "std_pressure": u"Ecart type pression  sur le papier",
            "average_acceleration": u"Accéleration moyenne",
            "renyi_entropy": u"Entropie de Rényi",
            "nb_breaks": u"Nombre de pauses > 35 ms",
            "average_time_breaks": u"Durée moyenne des pauses > 35 ms",
            "snvpd": u"SNVPD",
            "snr_x": u"SNR CE x",
            "snr_y": u"SNR CE y",
            "nb_strokes": u"Nombre de traits",
            "nb_lifts": u"Nombre de levées",
            "nb_slow_move": u"Nombre de mouvements lents",
            "mean_slow_move": u"Moyenne durée des mouvements lents (ms)",
            "std_slow_move": u"Déviation standard mouvements lents",
            "mean_azimuths": u"Moyenne angle a1 (Azimut, en degree)",
            "mean_altitudes": u"Moyenne angle a3 (Altitude, en degree)",
            "cov_azimuths": u"Coefficient de variance angle a1 (Azimut)",
            "cov_altitudes": u"Coefficient de variance angle a3 (Altitude)",
        }

        self.SetScrollRate(5, 5)
        sizer = wx.FlexGridSizer(0, 2, 0, 0)
        sizer.AddGrowableCol(1)
        sizer.SetFlexibleDirection(wx.BOTH)
        sizer.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        # We will create texts and text control for the 18 results there are to calculate
        for key, value in self.fields_label.items():
            self.create_component(key, value, sizer)

        # This button will allow for the results to be saved in a file
        self.button_save = wx.Button(self, wx.ID_ANY, u"Enregistrer \nles résultats", wx.DefaultPosition,
                                     wx.DefaultSize, 0)
        sizer.Add(self.button_save, 0, wx.ALL, 5)
        self.SetSizer(sizer)
        self.Layout()
        sizer.Fit(self)
        self.Bind(wx.EVT_BUTTON, self.button, self.button_save)

    def calcul(self, debut, fin, snvpd_first_frequence, snvpd_second_frequence):
        # Thanks to this, we can either end on the end of the drawing or at the point the user wants
        if fin == -1:
            fin = len(self.record.data.raw_data)
        record = copy.deepcopy(self.record.data)
        if fin != -1 and debut != 0:
            # Filter row data to the range select
            record.raw_data = record.raw_data[debut:fin]
            record.update()
        self.fields['nb_lifts'].SetValue(str(record.nb_lifts))
        self.fields['nb_strokes'].SetValue(str(record.nb_strokes))

        # Get first contact of pen to the paper
        first_contact_on_paper = np.argmax(record.contact > 0)
        last_contact_on_paper = np.argwhere(record.contact > 0)[-1][0]

        # Total length
        self.set_total_length(first_contact_on_paper, last_contact_on_paper, record)

        # Length on the paper between start and end
        self.set_total_length_paper(record)

        # Total paper time
        self.set_total_time_paper( first_contact_on_paper, record)

        # Total air time
        self.set_total_time_air(first_contact_on_paper, record)

        # Total average speed
        self.set_total_average_speed( first_contact_on_paper, record)

        # Average speed on paper
        self.set_total_average_speed_paper( record)

        # Average total pressure
        self.set_pressure_average( record)


        # Pressure average on paper
        self.set_pressure_average_paper( record)

        # Average Acceleration
        self.set_average_acceleration( record)

        # Entropy
        self.set_entropy( record)

        # # Number of breaks
        self.set_nb_breaks( record)

        # SNVPD
        self.set_snvpd( record, snvpd_first_frequence, snvpd_second_frequence)

        # SNR
        self.set_snr( first_contact_on_paper, record)

        # Tilt changes
        self.set_angles( record)

        # Slow movements
        self.set_slow_movements( record)

    # Saving the results in a file
    def button(self, evt):

        # Retrieving information
        df = pd.DataFrame(
            columns=['Subject_ID', 'Annotation', 'Laterality', 'Gender', 'Age_Days', 'Subject_class', 'Session_date',
                     'Session_tools','Stimulus'])
        df.loc[0, 'Subject_ID'] = self.record.data.subject.ID
        df.loc[0, 'Annotation'] = self.record.data.subject.annotation
        df.loc[0, 'Laterality'] = self.record.data.subject.laterality
        df.loc[0, 'Gender'] = self.record.data.subject.gender
        df.loc[0, 'Age_Days'] = self.record.data.subject.age
        df.loc[0, 'Subject_class'] = self.record.data.subject.grade
        df.loc[0, 'Session_date'] = self.record.data.subject.session_date
        df.loc[0, 'Session_tools'] = self.record.data.subject.tablet
        df.loc[0, 'Stimulus'] = self.record.data.subject.pool

        # Retrieving results
        for key, value in self.fields_label.items():
            df.loc[0, value] = self.fields[key].GetLineText(0)

        subject = self.record.data.subject
        # Opening the address of a file and writing the results
        with wx.FileDialog(self, "Enregistrer le fichier", wildcard="CSV files (*.csv)|*.csv",
                           style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT, defaultFile='Export_' + subject.ID + '_' + subject.filename) as fileDialog:

            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return  # The user changed their mind
            # Save the current contents in the file
            pathname = fileDialog.GetPath()
            try:
                df.to_csv(pathname, index=False, header=True, sep=";", quoting=csv.QUOTE_ALL)
            except IOError:
                wx.LogError("Cannot save current data in file '%s'." % pathname)

    def create_component(self, key, label, sizer):
        self.fields['label_' + key] = wx.StaticText(self, wx.ID_ANY, label, wx.DefaultPosition,
                                                    wx.DefaultSize, 0)
        self.fields['label_' + key].Wrap(-1)
        sizer.Add(self.fields['label_' + key], 0, wx.ALL, 5)

        self.fields[key] = wx.TextCtrl(self, wx.ID_ANY, u"", wx.DefaultPosition, wx.DefaultSize, 0)
        sizer.Add(self.fields[key], 0, wx.ALL | wx.EXPAND, 5)


    def set_total_length(self, first_contact_on_paper, last_contact_on_paper, record, ):
        print('Begin total_length ')
        x = record.x[first_contact_on_paper:last_contact_on_paper]
        y = record.y[first_contact_on_paper:last_contact_on_paper]
        length = np.sum(np.sqrt((x[1:] - x[:-1]) ** 2 + (y[1:] - y[:-1]) ** 2))
        self.fields['total_length'].SetValue(str(round(length, 3)))
        print('Finish total_length ')


    def set_total_length_paper(self, record):
        print('Begin total_length_paper ')
        length_paper = 0
        for stroke in record.strokes_nf:
            # We calculate the distance traveled by the pen between two samples if the pressure is different from 0
            length_paper = length_paper + np.sum(
                np.sqrt((stroke[1:, 0] - stroke[:-1, 0]) ** 2 + (stroke[1:, 1] - stroke[:-1, 1]) ** 2))
        self.fields['total_length_paper'].SetValue(str(round(length_paper, 3)))
        print('Finish total_length_paper ')


    def set_total_time_paper(self, first_contact_on_paper, record):
        print('Begin  total_time_paper')
        total_time_paper = 0
        indices = np.argwhere(record.contact == 1)
        last_index = 0 if indices.size == 0 else indices[-1][0]
        if last_index != 0:
            for i in range(first_contact_on_paper + 1, last_index):
                if record.contact[i] == 1:
                    total_time_paper = total_time_paper + (record.t[i] - record.t[i - 1])
        self.fields['total_time_paper'].SetValue(str(int(total_time_paper)))
        print('Finish  total_time_paper')


    def set_total_time_air(self, first_contact_on_paper, record):
        print('Begin total_time_air ')
        total_time_air = 0
        indices = np.argwhere(record.contact == 0)
        last_index = 0 if indices.size == 0 else indices[-1][0]
        if last_index != 0:
            for i in range(first_contact_on_paper + 1, last_index):
                if record.contact[i] == 0:
                    total_time_air = total_time_air + (record.t[i] - record.t[i - 1])
        self.fields['total_time_air'].SetValue(str(int(total_time_air)))
        print('Finish total_time_air ')


    def set_total_average_speed(self, first_contact_on_paper, record):
        print('Begin total_average_speed ')
        average_speed = record.speed[first_contact_on_paper:]
        self.fields['total_average_speed'].SetValue(str(round(np.mean(average_speed), 3)))
        print('Finish total_average_speed ')


    def set_total_average_speed_paper(self, record):
        print('Begin total_average_speed_paper ')
        average_speed_paper = record.speed[np.argwhere(record.contact > 0)][:,0]
        self.fields['total_average_speed_paper'].SetValue(str(round(np.mean(average_speed_paper),3)))
        print('Finish total_average_speed_paper ')


    def set_pressure_average(self, record):
        print('Begin  pressure_average')
        self.fields['pressure_average'].SetValue(str(round(np.mean(record.pressure), 3)))
        print('Finish  pressure_average')


    def set_pressure_average_paper(self, record):
        print('Begin pressure_average_paper ')
        pressure_average_paper = record.pressure[np.argwhere(record.contact > 0)][:,0]
        self.fields['pressure_average_paper'].SetValue(str(round(np.mean(pressure_average_paper),3)))
        self.fields['std_pressure'].SetValue(str(round(np.std(pressure_average_paper), 3)))
        print('Finish pressure_average_paper ')


    def set_average_acceleration(self, record):
        print('Begin  average_acceleration')
        self.fields['average_acceleration'].SetValue(str(round(np.mean(record.accs), 3)))
        print('Finish  average_acceleration')


    def set_entropy(self, record):
        print('Begin entropy')
        entropy = RenyiEntropy.process(record)
        self.fields['renyi_entropy'].SetValue(entropy)
        print('Finish entropy')


    def set_nb_breaks(self, record):
        print('Begin nb_breaks')
        nb_break = LongStops.process(record)
        self.fields['nb_breaks'].SetValue(nb_break['nb_breaks'])
        self.fields['average_time_breaks'].SetValue(nb_break['average_time_breaks'])
        print('Finish nb_breaks')


    def set_snvpd(self, record, snvpd_first_frequence, snvpd_second_frequence):
        print('Begin snvpd ')
        try:
            snvpd = Snvpd.process(record, snvpd_first_frequence, snvpd_second_frequence)
            self.fields['snvpd'].SetValue(str(snvpd))
        except Exception as e:
            print(e)
        print('Finish snvpd ')


    def set_snr(self, first_contact_on_paper, record):
        print('Begin snr ')
        snr = Snr.process(first_contact_on_paper, record, record.nb_strokes)
        self.fields['snr_x'].SetValue(snr['snr_x'])
        self.fields['snr_y'].SetValue(snr['snr_y'])
        print('Finish snr')


    def set_angles(self, record):
        print('Begin set_angles')
        # retrieve the a1 and a3 if the pressure at this time stamp is > 0
        azimuths = record.ax[record.pressure > 0]
        altitudes = record.az[record.pressure > 0]

        # cov : Pen tilt Coefficient Of Variance
        # cov = std / mean
        mean_azimuths = np.mean(azimuths)
        mean_altitudes = np.mean(altitudes)
        std_azimuths = np.nanstd(azimuths)
        std_altitudes = np.nanstd(altitudes)
        cov_azimuths = std_azimuths / mean_azimuths
        cov_altitudes = std_altitudes / mean_altitudes

        self.fields['mean_azimuths'].SetValue(str(round(mean_azimuths/10, 3)))
        self.fields['mean_altitudes'].SetValue(str(round(mean_altitudes/10, 3)))
        self.fields['cov_azimuths'].SetValue(str(round(cov_azimuths, 3)))
        self.fields['cov_altitudes'].SetValue(str(round(cov_altitudes, 3)))


        print('End set_angles')


    def set_slow_movements(self, record):
        print('Begin slow_movements ')
        slow_move = SlowMovements.process(record)
        self.fields['nb_slow_move'].SetValue(slow_move['nb_slow_move'])
        self.fields['mean_slow_move'].SetValue(slow_move['mean_slow_move'])
        self.fields['std_slow_move'].SetValue(slow_move['std_slow_move'])
        print('Finish slow_movement')

def get_type(path):
    try:
        return path[path.rindex('.') + 1:]
    except ImportError:
        return None


def get_n(filename, type_of_file):
    try:
        result = ""
        if type_of_file == "csv" :
             result = filename[filename.rindex('_') + 1:filename.rindex('.')]
        else:
            result = int(filename[filename.rindex('_') -1]) - 1
        return str(result)
    except ImportError:
        return None


def get_txt_id(path):
    try:
        path_array = path.split('\\')
        filename = path_array[-1]
        return str(filename.split('_')[0])
    except ImportError:
        return None

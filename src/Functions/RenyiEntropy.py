import json

import numpy as np
from math import pi, sqrt, log, exp


def process(record):
    """
    Extract the Renyi Entropy of the signals
    :param record: record
    :return:
            Renyi_Entropy_2_x       : order 2 Renyi Entropy along the x axis
            Renyi_Entropy_2_y       : order 2 Renyi Entropy along the y axis
            Renyi_Entropy_2_2d      : order 2 Renyi Entropy
            Std_Renyi_Entropy_2_x       : std of the order 2 Renyi Entropy along the x axis
            Std_Renyi_Entropy_2_y       : std of the order 2 Renyi Entropy along the y axis
            Std_Renyi_Entropy_2_2d      : std of the order 2 Renyi Entropy
    """

    def entropy(p, r):
        iterable = [p ** r for p in p]
        h = 1 / (1 - r) * log(sum(iterable))
        return h

    def normal(x, mu, sigma):
        if sigma == 0:
            return 1
        else:
            return 1 / (sqrt(2 * pi * sigma ** 2)) * exp(-(x - mu) ** 2 / (2 * sigma ** 2))

    def normal_2d(x, y, mu_x, mu_y, sigma):
        if sigma == 0:
            return 1
        else:
            dist_mu_square = (x - mu_x) ** 2 + (y - mu_y) ** 2
            return 1 / (2 * pi * sigma ** 2) * exp(-dist_mu_square / (2 * sigma ** 2))

    try:
        tot_h_2_x = 0
        tot_h_2_y = 0
        tot_h_2_2d = 0
        std_h_2_x = 0
        std_h_2_y = 0
        std_h_2_2d = 0
        mean_h_2_x = 0
        mean_h_2_y = 0
        mean_h_2_2d = 0
        if record.nb_strokes > 1:
            # "Naive" computation of the Renyi entropy"
            p_x, p_y, p_2d = [], [], []
            h_2_x_strokes, h_2_y_strokes, h_2_2d_strokes = [], [], []
            for idx, stroke in enumerate(record.strokes):
                t, x, y = stroke[:, 2], stroke[:, 0], stroke[:, 1]
                n = len(x)
                if n > 30:
                    # compute the durations between each point
                    dt = np.diff(t)
                    # compute the predicted position of the next point considering the movement is uniform
                    predicted_x = dt[1:] / dt[:-1] * x[1:-1] + (x[1:-1] - x[:-2])
                    predicted_y = dt[1:] / dt[:-1] * y[1:-1] + (y[1:-1] - y[:-2])

                    # compute the p_i for each point i
                    proba_x = [normal(predicted_x[i], x[i + 2], (x[i + 2] - x[i + 1]) / 2) for i in range(n - 2)]
                    proba_y = [normal(predicted_y[i], y[i + 2], (y[i + 2] - y[i + 1]) / 2) for i in range(n - 2)]
                    proba_2d = [normal_2d(predicted_x[i], predicted_y[i], x[i + 2], y[i + 2],
                                          sqrt((x[i + 2] - x[i + 1]) ** 2 + (y[i + 2] - y[i + 1]) ** 2) / 2) for i in
                                range(n - 2)]
                    p_x.extend(proba_x)
                    p_y.extend(proba_y)
                    p_2d.extend(proba_2d)
                    h_2_x_strokes.append(entropy(proba_x, 2))
                    h_2_y_strokes.append(entropy(proba_y, 2))
                    h_2_2d_strokes.append(entropy(proba_2d, 2))
            # compute the entropies
            tot_h_2_x = entropy(p_x, 2)
            tot_h_2_y = entropy(p_y, 2)
            tot_h_2_2d = entropy(p_2d, 2)

            h_2_x_strokes = np.array(h_2_x_strokes)
            h_2_y_strokes = np.array(h_2_y_strokes)
            h_2_2d_strokes = np.array(h_2_2d_strokes)

            std_h_2_x = np.std(h_2_x_strokes)
            std_h_2_y = np.std(h_2_y_strokes)
            std_h_2_2d = np.std(h_2_2d_strokes)

            mean_h_2_x = np.mean(h_2_x_strokes)
            mean_h_2_y = np.mean(h_2_y_strokes)
            mean_h_2_2d = np.mean(h_2_2d_strokes)

        return json.dumps({
            "Renyi Entropy 2 x": tot_h_2_x,
            "Renyi Entropy 2 y": tot_h_2_y,
            "Renyi Entropy 2 2d": tot_h_2_2d,
            "Std Renyi Entropy 2 x": std_h_2_x,
            "Std Renyi Entropy 2 y": std_h_2_y,
            "Std Renyi Entropy 2 2d": std_h_2_2d,
            "Mean Renyi Entropy 2 x": mean_h_2_x,
            "Mean Renyi Entropy 2 y": mean_h_2_y,
            "Mean Renyi Entropy 2 2d": mean_h_2_2d
        })
    except:
        return "Erreur dans le calcul"

"""
    Description :
        Calculates the Signal-to-Noise velocity peaks difference (SNvpd).
        See article "Signal-to-Noise velocity peaks difference: A new method for evaluating the handwriting movement fluency in children with dysgraphia"
        written by Danna et al. for more details.
"""

import numpy as np
from scipy import signal
from scipy.interpolate import interp1d


def process(record, first_frequence, second_frequence):
    if record.nb_strokes > 0:
        snvpd_f = 0
        for stroke in record.strokes_nf:
            if len(stroke) > 25:  # avoid calculation for short strokes
                interval = 5
                first_t, first_x, first_x_filter, first_y, first_y_filter = resample_and_filter(t=stroke[:, 2],x=stroke[:, 0], y=stroke[:, 1], interval=interval, order=4, fc=first_frequence)
                second_t, second_x, second_x_filter, second_y, second_y_filter = resample_and_filter(t=stroke[:, 2],x=stroke[:, 0],y=stroke[:, 1],interval=interval,order=4,fc=second_frequence)
                first_delta_l = np.sqrt((first_x_filter[1:] - first_x_filter[:-1]) ** 2 + (first_y_filter[1:] - first_y_filter[:-1]) ** 2)
                second_delta_l = np.sqrt((second_x_filter[1:] - second_x_filter[:-1]) ** 2 + (second_y_filter[1:] - second_y_filter[:-1]) ** 2)

                first_speed = first_delta_l / interval
                second_speed = second_delta_l / interval

                first_acceleration = np.diff(first_speed) / interval
                second_acceleration = np.diff(second_speed) / interval

                first_acceleration_times = first_acceleration[:-1] * first_acceleration[1:]
                second_acceleration_times = second_acceleration[:-1] * second_acceleration[1:]

                first_where = np.where(first_acceleration_times < 0)
                second_where = np.where(second_acceleration_times < 0)
                first_where_final = []
                second_where_final = []
                for i in second_where[0]:
                    if second_acceleration[i] > 0:
                        second_where_final.append(i + 1)
                for i in first_where[0]:
                    if first_acceleration[i] > 0:
                        first_where_final.append(i + 1)
                first_count = np.size(first_where_final)
                second_count = np.size(second_where_final)
                if first_count - second_count >= 0:
                    snvpd_stroke_f = first_count - second_count
                else:
                    snvpd_stroke_f = 0
            else:
                snvpd_stroke_f = 0
            snvpd_f = snvpd_f + snvpd_stroke_f
        return snvpd_f
    else:
        return 0


def resample_and_filter(t, x, y=[], interval=5, order=4, fc=15):
    """Helper that makes a linear interpolation of the given signal (t,x) in order to obtain a proper signal regurlarly sampled.
       Then it filters the signal with a low pass butterworth filter of a given order.
      :param t: times of the x data (ms)
      :param x: data to be filtered (x coordinate for example)
      :param y: data to be filtered (y coordinate for example), not mandatory
      :param interval: interval desired (ms) for the new signal (5 if not given)
      :param order: order of the butterworth low pass filter (4 if not given)
      :param fc: Cut off frequency (Hz) for the low pass butterworth filter (15 if not given)
      :return: tnew : time vector (ms)
               x_sampled : x data for each time value, not filtered
               x_sampled_and_filtered : x data for each time value, sampled and filtered
               y_sampled : y data for each time value, not filtered, [] if no y in input
               y_sampled_and_filtered : y data for each time value, sampled and filtered, [] if no y in input
    """

    tnew = np.arange(t[0], t[-1], interval)
    if t[-1] - tnew[-1] >= interval:
        tnew = np.concatenate((tnew, [tnew[-1] + interval]))
    # Design the Buterworth filters (Cutoff frequencies 10Hz and 5Hz)
    N = order  # Filter order for both filters
    Fc = fc  # Cutoff frequency
    F_acq = 1000 / interval  # Frequency at which data has been sampled

    Wn = Fc / (F_acq / 2)  # Normalized cutoff frequency

    B, A = signal.butter(N, Wn, btype='lowpass', output='ba')

    fx = interp1d(t, x, kind='linear')
    x_sampled = fx(tnew)
    x_sampled_and_filtered = signal.filtfilt(B, A, x_sampled)

    if y == []:
        y_sampled = []
        y_sampled_and_filtered = []
    else:
        fy = interp1d(t, y, kind='linear')
        y_sampled = fy(tnew)
        y_sampled_and_filtered = signal.filtfilt(B, A, y_sampled)

    return tnew, x_sampled, x_sampled_and_filtered, y_sampled, y_sampled_and_filtered

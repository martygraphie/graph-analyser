"""
Description :
    Count the number of times there is a slow movement (less than 1mm in 150ms), their mean and std duration,
    the slow move length / writing length ratio and slow move time / writing time ratio

"""
import numpy as np


def process(rec):
    """
    Extract the number slow movements, their mean duration and std
    :param fields_value:
    :param rec: record
    :return: Number_slow_mov : number of slow movements
             Mean_slow_mov : mean duration of slow movements
             Std_slow_mov : Std of slow movements duration
    """
    field_value = {}
    # %%
    # According to Caroline Jolly, a slow movement is when less than 1mm is drawn in 150ms.
    # Co compute it, we look for 150ms during which less than 1mm is drawn, and we add all the following points with speed < 1/150mm/ms
    speed_limit = 1 / 150

    if len(rec.strokes) > 0:
        # Interval will be used to resample the data
        interval = 5

        # Initialisation of the variables that will be used
        series = {}
        slow_lengths = {}
        slow_times = {}
        k = 0
        previous_stroke_length = 0

        for idx, stroke in enumerate(rec.strokes):  # Going through all the strokes
            if len(stroke) > 35:  # avoid calculation for short strokes

                # Resampling with 5ms timestamp and filtering with Fc = 10Hz
                # tnew, xnew, xf, ynew, yf = resample_and_filter(t = stroke[:,0], x = stroke[:,1],y = stroke[:,2],
                #                                                              interval = interval, order = 4, fc = 10)

                # Calculating distance between points
                # delta_l = np.sqrt(np.diff(xf)**2 + np.diff(yf)**2)

                # Calculating speed
                # speed = delta_l/interval

                speed_filtered = rec.speed[rec.contact > 0]
                speed = speed_filtered[previous_stroke_length:previous_stroke_length + len(stroke) - 1]
                delta_l = speed * np.diff(stroke[:, 0])
                previous_stroke_length = len(stroke)

                # Initialisation of the indexes of dists_modif the algorithm has already seen
                index_seen = np.array([])
                # Initialising other variables needed
                i = 1
                prov = np.array([])

                # Looking for 150ms where less than 1mm has been done
                for index in range(0, len(delta_l) - 30):  # Going through all the points until end-30
                    if index not in index_seen:  # If it has not been considered yet
                        # if during the 150ms after this point, less than 1mm is made
                        if np.sum(delta_l[index:index + 30]) < 1:  # 30 points = 150 ms
                            i = 1
                            # Adding all these points to "prov" and "index_seen" to not consider them again
                            prov = np.arange(index, index + 30)
                            index_seen = np.concatenate((index_seen, np.arange(index, index + 30)))

                            # Looking to all the points after the ones just considered and checking if speed < 1/150
                            # We stop as soon as the speed between two points is more than 1/150
                            while speed[index + 29 + i] < speed_limit:
                                index_seen = np.concatenate((index_seen, [index + 29 + i]))
                                prov = np.concatenate((prov, [index + 29 + i]))
                                i = i + 1
                                if index + 29 + i >= len(speed):
                                    break
                            # Saving the points
                            if len(series) == 0:
                                series[k] = prov
                                slow_times[k] = len(series[k]) * interval
                                slow_lengths[k] = np.sum(delta_l[series[k]])
                            # If the first point of new prov is just after the last point of the previous one, we stick
                            # them in order not to create two "slow movements'
                            elif series[k][-1] == prov[0] - 1:
                                series[k] = np.concatenate((series[k], prov[1:]))
                                slow_times[k] = len(series[k]) * interval
                                slow_lengths[k] = np.sum(delta_l[series[k]])
                            else:
                                k += 1
                                series[k] = prov
                                slow_times[k] = len(series[k]) * interval
                                slow_lengths[k] = np.sum(delta_l[series[k]])
            else:
                pass

        # Extraction of outputs :
        num = len(series)
        if len(list(slow_times.values())) > 0:  # If there is at least one slow movement
            mean = np.mean(list(slow_times.values()))
            std = np.std(list(slow_times.values()))
        else:  # If there is no slow movement
            mean = 0
            std = 0

        # %%
        field_value['nb_slow_move'] = str(round(num, 3))
        field_value['mean_slow_move'] = str(round(mean, 3))
        field_value['std_slow_move'] = str(round(std, 3))
    else:
        field_value['nb_slow_move'] = str(0)
        field_value['mean_slow_move'] = str(0)
        field_value['std_slow_move'] = str(0)

    return field_value
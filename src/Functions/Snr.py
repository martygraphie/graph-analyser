"""
    File : SNR_CE
    Project : Ecriture
    Author : Louis DEVILLAINE
    Description : Computes the Signal to Noise Ratio based on conventional energy
                  cf. "Garcia, Robust smoothing of gridded data in one and higher dimensions with missing values", 2009
"""
import numpy as np
from numpy.fft import fft, ifft


def process(first_contact_on_paper, record, nb_strokes):
    """
    Extract the Signal to noise ratio
    what is considered to be noise here is the difference between the raw input and the smoothed signal
    :param nb_strokes:
    :param record:
    :param first_contact_on_paper:
    :param fields_value:
    :param rec: record
    :return:
            snr_ce_x        : signal to noise ratio using conventional energy along the x axis
            snr_ce_y        : signal to noise ratio using conventional energy along the y axis
    """
    field_value = {}
    if record.nb_strokes > 0:
        x_all, y_all = [], []
        smooth_x_all, smooth_y_all = [], []
        for idx, stroke in enumerate(record.strokes):
            raw_x = stroke[:, 1]
            raw_y = stroke[:, 2]
            n = len(raw_x)
            if n > 50:  # we only smooth the strokes which are long enough because the others cannot be smoothed correctly
                x = (raw_x - np.mean(raw_x)) / np.std(raw_x)
                y = (raw_y - np.mean(raw_y)) / np.std(raw_y)
                length_pad = int(n / 8)
                n_pad = n + 2 * length_pad
                x_pad = np.pad(x, length_pad, mode='edge')
                y_pad = np.pad(y, length_pad, mode='edge')
                fft_x, fft_y = fft(x_pad, norm='ortho'), fft(y_pad, norm='ortho')
                gamma = np.zeros((n_pad, n_pad))
                window = np.hanning(n_pad)
                for i in range(n_pad):
                    gamma[i][i] = 1 / (1 + 1000 * window[i])
                # smoothed_strokes
                smooth_x, smooth_y = ifft(gamma.dot(fft_x), norm='ortho'), ifft(gamma.dot(fft_y), norm='ortho')
                smooth_x_all.extend([smooth_x[i] for i in range(n_pad) if length_pad <= i < n + length_pad])
                smooth_y_all.extend([smooth_y[i] for i in range(n_pad) if length_pad <= i < n + length_pad])
                x_all.extend(x)
                y_all.extend(y)
        n = len(x_all)
        if n > 0:
            energy_x, noise_x = 1, sum(np.abs([(smooth_x_all[i] - x_all[i]) ** 2 for i in range(n)]))
            energy_y, noise_y = 1, sum(np.abs([(smooth_y_all[i] - y_all[i]) ** 2 for i in range(n)]))
            snr_ce_x, snr_ce_y = 1 / n * energy_x / noise_x, 1 / n * energy_y / noise_y
            field_value['snr_x'] = '{:0.3e}'.format(snr_ce_x)
            field_value['snr_y'] = '{:0.3e}'.format(snr_ce_y)
    else:
        field_value['snr_x'] = "0"
        field_value['snr_y'] = "0"

    return field_value

"""
    File : Stops_35ms
    Project : Ecriture
    Author : Louis Deschamps
    Description :
        Extracts the number of on-paper stops longer than 35ms.
        Extracts the mean duration and the standard deviation of abnormal stops (>35ms, pen on the paper).
"""
import numpy as np
import pandas as pd


def process(record):
    value = {}
    count = 0
    durations_average = 0
    tab_stop = pd.DataFrame(columns=['duration'])
    if record.nb_strokes > 0:
        for stroke in record.strokes:
            tab_dist = pd.DataFrame(columns=['time', 'distance'])
            distances = np.sqrt((stroke[1:, 0] - stroke[:-1, 0]) ** 2 + (stroke[1:, 1] - stroke[:-1, 1]) ** 2)
            if distances.size > 0:
                # If the distance between two points is lower than 0.001 mm, we consider that there hasn't been movement
                tab_dist['distance'] = [dist if dist > 0.001 else 0 for dist in distances]
                tab_dist['time'] = np.vstack((stroke[1:, 2] - stroke[:-1, 2]))

                duration = 0
                for index, row in tab_dist.iterrows():
                    # If the distance is equal to 0 we add the value of the time between the two samples where the
                    # distance is equal to 0 to the already existing time value. Else if the time is greater than 35 ms,
                    # then there is one pause and we reset the time at 0.
                    if row['distance'] == 0:
                        duration = duration + row['time']
                    else:
                        if duration >= 35:
                            tab_stop.loc[count, 'duration'] = duration
                            count += 1
                        duration = 0
        if count > 0:
            durations_average = round(tab_stop['duration'].mean(), 3)
    value['nb_breaks'] = str(count)
    value['average_time_breaks'] = str(durations_average)
    return value

import sys
import traceback

import wx


class Canvas(wx.ScrolledWindow):
    def __init__(self, parent, id=-1, size=wx.DefaultSize, scroll=True):
        wx.ScrolledWindow.__init__(self, parent, id, (0, 0), size=size, style=wx.SUNKEN_BORDER)
        self.SetBackgroundColour("WHITE")
        self.SetCursor(wx.Cursor(wx.CURSOR_CROSS))
        self.scroll = scroll

        self.drawingWidth, self.drawingHeight = 0, 0
        self.maxWidth, self.maxHeight = 2000, 2000
        self.SetScrollRate(2, 2)

        # Initialize the buffer bitmap.  No real DC is needed at this point.
        self.buffer = wx.Bitmap(self.maxWidth, self.maxHeight)
        dc = wx.BufferedDC(None, self.buffer)
        dc.Clear()

        self.Bind(wx.EVT_PAINT, self.on_paint)
        self.Bind(wx.EVT_SIZE, self.on_size)

    def on_paint(self, event):
        wx.BufferedPaintDC(self, self.buffer, wx.BUFFER_VIRTUAL_AREA)

    def draw(self, dc):
        # this function must be overloaded
        pass

    def update(self):
        # Update the canvas and the buffer
        self.SetVirtualSize((self.drawingWidth, self.drawingHeight if self.scroll else 0))
        self.buffer = wx.Bitmap(self.maxWidth, self.maxHeight)
        dc = wx.BufferedDC(None, self.buffer)
        self.draw(dc)
        self.Refresh()

    def on_size(self, event):
        # figure out what part of the window to refresh, based
        # on what parts of the buffer we just updated
        dc = wx.BufferedDC(None, self.buffer)
        x1, y1, x2, y2 = dc.GetBoundingBox()
        x1, y1 = self.CalcScrolledPosition(x1, y1)
        x2, y2 = self.CalcScrolledPosition(x2, y2)

        # make a rectangle
        rect = wx.Rect()
        rect.SetTopLeft((x1, y1))
        rect.SetBottomRight((x2, y2))
        rect.Inflate(2, 2)

        # refresh it
        self.RefreshRect(rect)

def main():
    app = wx.App(False)
    app.MainLoop()

if __name__ == '__main__':
    main()
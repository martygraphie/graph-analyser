"""
Parameters
------------
time_stamp : current time for each point

is_empty : record has sufficient information

passed : true if the person has done the stimuli

strokes : list of strokes

lifts : list of lifts

nb_strokes : number of strokes

nb_lifts : number of lifts

concat_strokes : all strokes gathered

barycenter : the position of the barycenter of the drawing

centered : all strokes centered and gathered

stimuli_name : name of the simuli

subject_ID : ID of the subject

subject_age : Age (days) of the subject

subject_laterality : laterality of the subject (0 for righ handed, 1 for left)

subject_gender : gender of the subject (0 for boy, 1 for girl)

subject_annotation : annotation of the subject (0 for unknown 1 for dysgraphic, 2 for no dysgraphic, 3 for typical)

tool_ID = ID of the tablet used for the session

filter : 'No filter' if the record has not been filtered, name of the filter if it has been

speeds : speed vector for every stroke. Calculated in the filters (speeds[:,0]:timestamps, speeds[:,1]:norm of the

speed vector, speeds[:,2]:x coordinate of speed vector, speeds[:,3]:y coordinate of speed vector)

accs : acceleration vector for every stroke. Calculated in the filters (cf speeds for description)

strokes_nf : unfiltered strokes

lifts_nf : unfiltered lifts

concat_strokes_nf = unfiltered concat_strokes

------
"""

import os
import pandas as pd
import numpy as np
from scipy.signal import butter, filtfilt
from scipy.spatial.distance import pdist, squareform


class DataRecord:
    def __init__(self, ):
        from src.Subject import Subject
        self.frequence_hz = 15
        self.interval = 5
        self.size = 0
        self.raw_data = None
        self.t = []
        self.x = []
        self.y = []
        self.z = []
        self.ax = []
        self.ay = []
        self.az = []
        self.pressure = []
        self.contact = []
        self.strokes_nf = []
        self.concat_strokes_nf = ''
        self.concat_strokes = ''
        self.lifts_nf = []
        self.strokes = []
        self.lifts = []
        self.nb_strokes = 0
        self.nb_lifts = 0
        self.speed = []
        self.accs = []
        self.passed = True
        self.barycenter = None
        self.centered = None
        self.stimuli_name = None
        self.subject = Subject()
        self.session_path = None
        self.type_of_file = ''
        self.is_empty = True
        self.data = None  # contains all the data from the raw file

    def set_frequence_hz(self, frequence_hz):
        self.frequence_hz = frequence_hz

    def init_record(self, path):
        from src.Utils import get_type
        self.type_of_file = get_type(path).lower()
        self.session_path = os.path.dirname(path)
        self.subject.set(path)

        if self.type_of_file == 'txt':
            self.from_txt(path)
        elif self.type_of_file == 'csv':
            self.from_csv(path)
        self.update()

    def update(self):
        self.update_data()
        self.size = len(self.raw_data)
        if self.size > 20: self.is_empty = False

        if not self.is_empty:
            self.strokes_nf, self.lifts_nf = self.decompose(self.raw_data)
            self.strokes, self.lifts = self.decompose(
                pd.DataFrame(list(zip(self.t, self.x, self.y, self.contact)), columns=['t', 'x', 'y', 'contact']))
            self.nb_strokes = self.strokes.shape[0]
            try:
                self.concat_strokes_nf = np.vstack(self.strokes_nf)
            except:
                self.concat_strokes_nf = self.strokes_nf
            try:
                self.concat_strokes = np.vstack(self.strokes)
            except:
                self.concat_strokes = self.strokes
            self.nb_lifts = self.lifts.shape[0]
            dist = pd.DataFrame(list(zip(self.t, self.x, self.y, self.z)), columns=['t', 'x', 'y', 'z']).diff().fillna(
                0.)
            dist['distance'] = np.sqrt(dist.x ** 2 + dist.y ** 2)
            dist['speed'] = dist.distance / (dist.t / 1e3)
            self.speed = np.nan_to_num(np.vstack(dist['speed']))
            dist['accs'] = (dist.speed - dist.speed.shift(1).fillna(0)) / dist.t
            self.accs = np.nan_to_num(np.vstack(dist['accs']))

    def from_csv(self, path):
        self.raw_data = pd.read_csv(path, skiprows=1, delimiter=",", encoding='latin1')
        self.raw_data.columns = self.raw_data.columns.str.replace(' ', '')
        self.raw_data.columns = map(str.lower, self.raw_data.columns)
        self.raw_data.columns = self.raw_data.columns.str.replace('time_stamp', 't')

        if self.subject.tablet == 1:
            self.raw_data['x'] /= 10
            self.raw_data['y'] /= 10

        elif self.subject.tablet == 2:
            self.raw_data['x'] /= 32.5
            self.raw_data['y'] /= 20.3
        elif self.subject.tablet == 3:
            self.raw_data['x'] /= 30.48
            self.raw_data['y'] /= 23.06
        else:
            self.raw_data['x'] /= 22.35
            self.raw_data['y'] /= 18.97

    def from_txt(self, path):
        try:
            self.raw_data = pd.read_csv(path, skiprows=2, sep='\t')
        except:
            self.raw_data = pd.read_csv(path, skiprows=2, encoding='latin1', sep='\t')
        self.raw_data.columns = self.raw_data.columns.str.replace(' ', '')
        self.raw_data = self.raw_data.drop(columns=["N°Pt", "SN"])
        self.raw_data.columns = self.raw_data.columns.str.replace('P', 'pressure')
        self.raw_data.columns = map(str.lower, self.raw_data.columns)
        if self.subject.tablet == 1:
            self.raw_data['x'] /= 10
            self.raw_data['y'] /= 10

        elif self.subject.tablet == 2:
            self.raw_data['x'] /= 32.5
            self.raw_data['y'] /= 20.3
        elif self.subject.tablet == 3:
            self.raw_data['x'] /= 30.48
            self.raw_data['y'] /= 23.06
        else:
            self.raw_data['x'] /= 22.35
            self.raw_data['y'] /= 18.97

    def update_data(self):
        if self.type_of_file == "csv":
            self.t = self.apply_filter(self.raw_data.loc[:, 't'])
            self.x = self.apply_filter(self.raw_data.loc[:, 'x'])
            self.y = self.apply_filter(self.raw_data.loc[:, 'y'])
            self.z = self.apply_filter(self.raw_data.loc[:, 'z'])
            self.ax = self.apply_filter(self.raw_data.loc[:, 'a1'])
            self.ay = self.apply_filter(self.raw_data.loc[:, 'a2'])
            self.az = self.apply_filter(self.raw_data.loc[:, 'a3'])
            self.pressure = self.apply_filter(self.raw_data.loc[:, 'pressure'])
            self.contact = np.vstack(self.raw_data.loc[:, 'contact'])
        else:
            if 't' in self.raw_data.columns:
                self.raw_data.t = [i * 5 for i in range(0, self.raw_data.shape[0])]
                self.t = self.apply_filter(self.raw_data.t)

            if 'x' in self.raw_data.columns:
                self.raw_data.x = self.raw_data.x / 100
                self.x = self.apply_filter(self.raw_data.x)
            if 'y' in self.raw_data.columns:
                self.raw_data.y = (max(self.raw_data.y) + (-1) * self.raw_data.y) / 100
                self.y = self.apply_filter(self.raw_data.y)
            if 'z' in self.raw_data.columns: self.z = self.apply_filter(self.raw_data.loc[:, 'z'])
            if 'pressure' in self.raw_data.columns:
                self.raw_data['contact'] = np.where(self.raw_data.pressure > 0, 1, 0)
                self.contact = np.vstack(self.raw_data['contact'])

            if 'pressure' in self.raw_data.columns:
                self.raw_data.pressure = self.raw_data.pressure / 1020
                self.pressure = self.apply_filter(self.raw_data.pressure)

            if 'alt' in self.raw_data.columns: self.az = self.apply_filter(self.raw_data.alt)
            if 'azi' in self.raw_data.columns: self.ax = self.apply_filter(self.raw_data.azi)

    def apply_filter(self, df):
        B, A = butter(4, self.frequence_hz, 'low', fs=200, output='ba')
        return filtfilt(B, A, df)

    def decompose(self, df):
        s1, s2 = [], []  # reset buf
        begin = df.first_valid_index()
        tmp = [[df.x[begin], df.y[begin], df.t[begin]]]
        for i in range(begin + 1, self.size):
            if df.contact[i] != df.contact[i - 1] or i >= self.size - 1:
                if df.contact[i - 1] > 0:
                    s1.append(tmp)
                else:
                    s2.append(tmp)
                tmp = []
            tmp.append([df.x[i], df.y[i], df.t[i]])

        return np.array([np.array(s) for s in s1]), np.array([np.array(s) for s in s2])

    def print_record(self):
        print(self.__dict__)
        self.subject.print_infos()
